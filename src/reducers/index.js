import { combineReducers } from 'redux'
import { songsReducer } from './SongsReducer';
import { selectedSongReducer } from './SelectedSongReducer';



export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});