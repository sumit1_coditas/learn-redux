export const songsReducer = () => {
    return [
        {title: 'Song 1' , duration: '4:05'},
        {title: 'Song 2' , duration: '3:30'},
        {title: 'Song 3' , duration: '3:00'},
        {title: 'Song 4' , duration: '4:30'},
        {title: 'Song 5' , duration: '5:30'}
    ];
};